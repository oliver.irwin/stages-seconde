import pygame
import random
import requests

from pygame.transform import scale

nom = "Théo"
groupe = 1
POST = False


class Serpent:
    def __init__(self):
        self.rect_tete = SPRITE_SERPENT_TETE_DROITE.get_rect()
        self.rect_tete.topleft = (largeur_zone * unite // 2, hauteur_zone * unite // 2)
        self.positions = [self.rect_tete.topleft]
        self.direction = (1, 0)
        self.vitesse = 10

    def actualiser_position(self):
        nouvelle_tete = (self.rect_tete.topleft[0] + self.direction[0] * unite,
                                  self.rect_tete.topleft[1] + self.direction[1] * unite)
        if nouvelle_tete[0] < 0:
            nouvelle_tete = (largeur_zone * unite - unite, nouvelle_tete[1])
        elif nouvelle_tete[0] >= largeur_zone * unite:
            nouvelle_tete = (0, nouvelle_tete[1])
        if nouvelle_tete[1] < 0:
            nouvelle_tete = (nouvelle_tete[0], hauteur_zone * unite - unite)
        elif nouvelle_tete[1] >= hauteur_zone * unite:
            nouvelle_tete = (nouvelle_tete[0], 0)

        teleport_pos = teleport.check_collision(nouvelle_tete)
        if teleport_pos is not None:
            nouvelle_tete = teleport_pos

        self.rect_tete.topleft = nouvelle_tete
        self.positions.append(self.rect_tete.topleft)

    def actualiser_direction(self, touche):
        if touche == pygame.K_UP and self.direction != (0, 1):
            self.direction = (0, -1)
        elif touche == pygame.K_DOWN and self.direction != (0, -1):
            self.direction = (0, 1)
        elif touche == pygame.K_LEFT and self.direction != (1, 0):
            self.direction = (-1, 0)
        elif touche == pygame.K_RIGHT and self.direction != (-1, 0):
            self.direction = (1, 0)

    def retirer_queue(self):
        self.positions.pop(0)

    def dessine(self, surface):
        prev_img = SPRITE_SERPENT_CORPS_HORIZONTAL
        for index_pos, pos in enumerate(self.positions):
            if index_pos == len(self.positions)-1:
                if self.direction == (1, 0):
                    img = SPRITE_SERPENT_TETE_DROITE
                elif self.direction == (-1, 0):
                    img = SPRITE_SERPENT_TETE_GAUCHE
                elif self.direction == (0, -1):
                    img = SPRITE_SERPENT_TETE_HAUT
                elif self.direction == (0, 1):
                    img = SPRITE_SERPENT_TETE_BAS
                else:  # ne devrait jamais arriver
                    img = SPRITE_SERPENT_TETE_DROITE
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)
            elif index_pos == 0:
                x, y = self.positions[index_pos]
                x_p1, y_p1 = self.positions[index_pos+1]
                if x < x_p1:
                    img = SPRITE_SERPENT_QUEUE_GAUCHE
                elif x > x_p1:
                    img = SPRITE_SERPENT_QUEUE_DROITE
                elif y < y_p1:
                    img = SPRITE_SERPENT_QUEUE_HAUT
                elif y > y_p1:
                    img = SPRITE_SERPENT_QUEUE_BAS
                else:
                    img = SPRITE_SERPENT_QUEUE_DROITE  # ne devrait jamais arriver
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)
            else:
                x_m1, y_m1 = self.positions[index_pos-1][0], self.positions[index_pos-1][1]
                x, y = self.positions[index_pos][0], self.positions[index_pos][1]
                x_p1, y_p1 = self.positions[index_pos+1][0], self.positions[index_pos+1][1]
                if y_m1 == y == y_p1:
                    img = SPRITE_SERPENT_CORPS_HORIZONTAL
                elif x_m1 == x == x_p1:
                    img = SPRITE_SERPENT_CORPS_VERTICAL
                elif (x_m1 == x < x_p1 and y_m1 > y == y_p1) or (x_m1 > x == x_p1 and y_m1 == y < y_p1):
                    img = SPRITE_SERPENT_CORPS_BAS_DROITE
                elif (x_m1 < x == x_p1 and y_m1 == y < y_p1) or (x_m1 == x > x_p1 and y_m1 > y == y_p1):
                    img = SPRITE_SERPENT_CORPS_GAUCHE_BAS
                elif (x_m1 < x == x_p1 and y_m1 == y > y_p1) or (x_m1 == x > x_p1 and y_m1 < y == y_p1):
                    img = SPRITE_SERPENT_CORPS_GAUCHE_HAUT
                elif (x_m1 == x < x_p1 and y_m1 < y == y_p1) or (x_m1 > x == x_p1 and y_m1 == y > y_p1):
                    img = SPRITE_SERPENT_CORPS_HAUT_DROITE
                else:
                    img = prev_img  # Teleport
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)
            prev_img = img

    def verifier_auto_collision(self):
        return self.positions[-1] in self.positions[:-1]


class Pomme(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = SPRITE_POMME
        self.rect = self.image.get_rect()
        self.rect.topleft = position_aleatoire()

    def dessine(self, surface):
        surface.blit(self.image, self.rect)

    def reposition(self):
        self.rect.topleft = position_aleatoire()

    def verifier_collision(self, pos: tuple) -> bool:
        return self.rect.collidepoint(pos)


class Teleport:
    def __init__(self):
        self.positions = [None, None]

    def draw(self, surface):
        for pos in self.positions:
            if pos is not None:
                img = SPRITE_PORTAL
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)

    def update(self):
        self.positions = (position_aleatoire(), position_aleatoire())

    def check_collision(self, pos: tuple):
        for i in range(2):
            if self.positions[i] is not None and self.positions[i] == pos:
                return self.positions[1 - i]  # Return the position of the other teleport block
        return None


class Obstacles(pygame.sprite.Sprite):

    FREQUENCE_PERMUTATION_OBSTACLES = 10

    def __init__(self):
        super().__init__()
        self.pos_obstacle = []
        self.num_obstacles = 0

    def actualiser_obstacles(self, score):
        self.num_obstacles = score - 4  # pour commencer à mettre des obstacles à partir de la 5eme pomme
        if score % self.FREQUENCE_PERMUTATION_OBSTACLES == 0:
            self.repositioner_obstacles()
        elif len(self.pos_obstacle) < self.num_obstacles:
            self.ajouter_obstacles()

    def ajouter_obstacles(self):
        difference = self.num_obstacles - len(self.pos_obstacle)
        for _ in range(difference):
            self.pos_obstacle.append(position_aleatoire())
        
    def repositioner_obstacles(self):
        self.pos_obstacle = []
        for _ in range(self.num_obstacles):
            self.pos_obstacle.append(position_aleatoire())

    def dessine(self, surface):
        for obs in self.pos_obstacle:
            img = SPRITE_OBSTACLE
            rect_obstacle = img.get_rect(topleft=obs)
            surface.blit(img, rect_obstacle)

    def verifier_collision(self, pos: tuple):
        return pos in self.pos_obstacle


def entier_aleatoire(nombre_max: int):
    return random.randint(0, nombre_max-1)


def position_aleatoire():
    return entier_aleatoire(largeur_zone) * unite, entier_aleatoire(hauteur_zone) * unite


def afficher_message(message: str, couleur: tuple):
    fenetre.blit(pygame.font.SysFont("comicsansms", 20).render(message, True, couleur), [0, 0])


def dans_zone(position: tuple):
    return (0 <= position[0] < largeur_zone*unite) and (0 <= position[1] < hauteur_zone*unite)


def dessine_fond():
    for y in range(0, hauteur_zone):
        for x in range(0, largeur_zone):
            color = vert_clair if (x + y) % 2 == 0 else vert_moins_clair
            pygame.draw.rect(fenetre, color, pygame.Rect(x * unite, y * unite, unite, unite))


def tour_de_jeu(condition_fin):
    dessine_fond()
    afficher_message(str(len(serpent.positions)-1), noir)
    serpent.dessine(fenetre)
    pomme.dessine(fenetre)
    obstacles.dessine(fenetre)
    teleport.draw(fenetre)
    pygame.display.update()

    for evenement in pygame.event.get():
        if evenement.type == pygame.QUIT:
            condition_fin = True
        if evenement.type == pygame.KEYDOWN:
            serpent.actualiser_direction(evenement.key)
    serpent.actualiser_position()

    tete_serpent = serpent.positions[-1]
    score_serpent = len(serpent.positions)-1
    if not dans_zone(tete_serpent) or obstacles.verifier_collision(tete_serpent) or serpent.verifier_auto_collision():
        condition_fin = True
    if pomme.verifier_collision(tete_serpent):
        pomme.reposition()
        serpent.vitesse += 0.75
        obstacles.actualiser_obstacles(score_serpent)
        if score_serpent > 10:
            teleport.update()
    else:
        serpent.retirer_queue()

    if condition_fin and POST:
        if POST:
            requests.post(
                "https://irwin.sh/stages/result",
                json={"name": nom, "score": len(serpent.positions)-1, "group": groupe}
            )

    return condition_fin


# Couleurs
noir = (0, 0, 0)
vert_clair = (176, 230, 40)
vert_moins_clair = (147, 199, 17)

# Variables
unite = 20
largeur_zone = 60
hauteur_zone = 40

# SPRITES
SPRITE_SERPENT_TETE_DROITE = scale(pygame.image.load("sprites/base_game/head_right.png"), (unite, unite))
SPRITE_SERPENT_TETE_GAUCHE = scale(pygame.image.load("sprites/base_game/head_left.png"), (unite, unite))
SPRITE_SERPENT_TETE_HAUT = scale(pygame.image.load("sprites/base_game/head_up.png"), (unite, unite))
SPRITE_SERPENT_TETE_BAS = scale(pygame.image.load("sprites/base_game/head_down.png"), (unite, unite))
SPRITE_SERPENT_CORPS_HORIZONTAL = scale(pygame.image.load("sprites/base_game/body_horizontal.png"), (unite, unite))
SPRITE_SERPENT_CORPS_VERTICAL = scale(pygame.image.load("sprites/base_game/body_vertical.png"), (unite, unite))
SPRITE_SERPENT_CORPS_GAUCHE_BAS = scale(pygame.image.load("sprites/base_game/body_bottomleft.png"), (unite, unite))
SPRITE_SERPENT_CORPS_GAUCHE_HAUT = scale(pygame.image.load("sprites/base_game/body_topleft.png"), (unite, unite))
SPRITE_SERPENT_CORPS_BAS_DROITE = scale(pygame.image.load("sprites/base_game/body_bottomright.png"), (unite, unite))
SPRITE_SERPENT_CORPS_HAUT_DROITE = scale(pygame.image.load("sprites/base_game/body_topright.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_DROITE = scale(pygame.image.load("sprites/base_game/tail_right.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_GAUCHE = scale(pygame.image.load("sprites/base_game/tail_left.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_HAUT = scale(pygame.image.load("sprites/base_game/tail_up.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_BAS = scale(pygame.image.load("sprites/base_game/tail_down.png"), (unite, unite))
SPRITE_POMME = scale(pygame.image.load("sprites/base_game/apple.png"), (unite, unite))
SPRITE_OBSTACLE = scale(pygame.image.load("sprites/base_game/obstacle.png"), (unite, unite))
SPRITE_GOLDEN = scale(pygame.image.load("sprites/base_game/golden_bonus.png") , (unite , unite))
SPRITE_PORTAL = scale(pygame.image.load("sprites/base_game/portal.png") , (unite , unite))

pygame.init()
pygame.mixer.init()
pygame.mixer.music.load("sons/CHILL.mp3")
pygame.mixer.music.set_volume(0.1)
pygame.mixer.music.play(-1)
pygame.display.set_caption("Snake")
fenetre = pygame.display.set_mode((largeur_zone*unite, hauteur_zone*unite))

fin_jeu = False
direction_serpent = (0, 0)
pomme = Pomme()
serpent = Serpent()
obstacles = Obstacles()
clock = pygame.time.Clock()
affichage_duree = 5
teleport = Teleport()

while not fin_jeu:
    fin_jeu = tour_de_jeu(fin_jeu)
    clock.tick(serpent.vitesse)

print("Game Over")
pygame.quit()
quit()
