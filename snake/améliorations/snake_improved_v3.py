import pygame
import random
import time


from pygame.transform import scale


class Serpent:
    def __init__(self):
        self.rect_tete = SPRITE_SERPENT_TETE_DROITE.get_rect()
        self.rect_tete.topleft = (largeur_zone * unite // 2, hauteur_zone * unite // 2)
        self.positions = [self.rect_tete.topleft]
        self.direction = (1, 0)

    def update(self):
        self.rect_tete.topleft = (self.rect_tete.topleft[0] + self.direction[0] * unite,
                                  self.rect_tete.topleft[1] + self.direction[1] * unite)
        self.positions.append(self.rect_tete.topleft)

    def draw(self, surface):
        for index_pos, pos in enumerate(self.positions):
            if index_pos == len(self.positions)-1:  # Tête
                if self.direction == (1, 0):
                    img = SPRITE_SERPENT_TETE_DROITE
                elif self.direction == (-1, 0):
                    img = SPRITE_SERPENT_TETE_GAUCHE
                elif self.direction == (0, -1):
                    img = SPRITE_SERPENT_TETE_HAUT
                elif self.direction == (0, 1):
                    img = SPRITE_SERPENT_TETE_BAS
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)
            elif index_pos == 0:  # Queue
                x, y = self.positions[index_pos]
                x_p1, y_p1 = self.positions[index_pos+1]
                if x < x_p1:
                    img = SPRITE_SERPENT_QUEUE_GAUCHE
                elif x > x_p1:
                    img = SPRITE_SERPENT_QUEUE_DROITE
                elif y < y_p1:
                    img = SPRITE_SERPENT_QUEUE_HAUT
                elif y > y_p1:
                    img = SPRITE_SERPENT_QUEUE_BAS
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)
            else:  # Corps
                x_m1, y_m1 = self.positions[index_pos-1][0], self.positions[index_pos-1][1]
                x, y = self.positions[index_pos][0], self.positions[index_pos][1]
                x_p1, y_p1 = self.positions[index_pos+1][0], self.positions[index_pos+1][1]
                if y_m1 == y == y_p1:
                    img = SPRITE_SERPENT_CORPS_HORIZONTAL
                elif x_m1 == x == x_p1:
                    img = SPRITE_SERPENT_CORPS_VERTICAL
                elif (x_m1 == x < x_p1 and y_m1 > y == y_p1) or (x_m1 > x == x_p1 and y_m1 == y < y_p1):
                    img = SPRITE_SERPENT_CORPS_BAS_DROITE
                elif (x_m1 < x == x_p1 and y_m1 == y < y_p1) or (x_m1 == x > x_p1 and y_m1 > y == y_p1):
                    img = SPRITE_SERPENT_CORPS_GAUCHE_BAS
                elif (x_m1 < x == x_p1 and y_m1 == y > y_p1) or (x_m1 == x > x_p1 and y_m1 < y == y_p1):
                    img = SPRITE_SERPENT_CORPS_GAUCHE_HAUT
                elif (x_m1 == x < x_p1 and y_m1 < y == y_p1) or (x_m1 > x == x_p1 and y_m1 == y > y_p1):
                    img = SPRITE_SERPENT_CORPS_HAUT_DROITE
                rect = img.get_rect(topleft=pos)
                surface.blit(img, rect)

    def set_direction(self, touche):
        if touche == pygame.K_UP:
            self.direction = (0, -1)
        elif touche == pygame.K_DOWN:
            self.direction = (0, 1)
        elif touche == pygame.K_LEFT:
            self.direction = (-1, 0)
        elif touche == pygame.K_RIGHT:
            self.direction = (1, 0)


class Pomme(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = SPRITE_POMME
        self.rect = self.image.get_rect()
        self.rect.topleft = position_aleatoire()

    def draw(self, surface):
        surface.blit(self.image, self.rect)

    def reposition(self):
        self.rect.topleft = position_aleatoire()


#### V3 : Ajout Obstacle - Bonus (Pomme Dorée) ####################################
class Pomme_Doree(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = SPRITE_GOLDEN
        self.rect = self.image.get_rect()
        self.rect.topleft = position_aleatoire()
        #Fréquence d'apparition de la pomme dorée qui est un multple de 5 du score
        self.frequence_or = 5
        #Cet attribut permet permet de déclencher le chrono pour afficher la pomme dorée pendant un intervalle de temps donnée
        self.pomme_or_active = False
       

    def draw(self, surface):
        surface.blit(self.image, self.rect)

    def reposition(self):
        self.rect.topleft = position_aleatoire()

#### Ajout Obstacle ############################################

class Obstacle(pygame.sprite.Sprite): 

    def __init__(self):
        super().__init__()
        self.pos_obstacle = []
        ##### V3 :  Cette variable permet de savoir si le serpent a touché la pomme dorée, si c'est le cas tous les obstacles deviennent des pommes
        self.change_bonus =False


        # Selon le score on augmente le nombre d'obstacle à chaque fois que le jour atteint un certain multiple de score
        self.num_obstacles = 1
        self.max_obstacles = 150

        # La frequence d'apparition des obstacles périodique d'un score qui est multiple d'un nombre choisi (ici 5)
        self.frequene_obstacle = 5
        # La frequence d'apparition du nombre obstacles périodique d'un score qui est multiple d'un nombre choisi (ici 8)
        self.frequene_obstacle_nombre = 8
        # La frequence de permutation des obstacles périodique d'un score qui est multiple d'un nombre choisi (ici 10)
        self.frequene_obstacle_perm = 10


    def update_new(self):  
        for _ in range(self.num_obstacles):
            self.pos_obstacle.append(position_aleatoire())
        
    def permute_obstacles(self): 
        serpent_passe = len(self.pos_obstacle)
        self.pos_obstacle = []
        for _ in range(serpent_passe):
            self.pos_obstacle.append(position_aleatoire())

    def draw_obstacle(self , surface): 

        for obstacle in self.pos_obstacle: 
            
            # Si l'attribut change_bonus est True alors on affiche les images de pommes, qui seront mangées par le serpent sinon les obstacles
            if self.change_bonus:
                img = SPRITE_POMME
            else: 
                img = SPRITE_OBSTACLE
            rect_obstacle = img.get_rect(topleft=obstacle)
            surface.blit(img , rect_obstacle)

    def collide_obstacle(self , pos : tuple):
        return pos in self.pos_obstacle 


        


def entier_aleatoire(max: int):
    return random.randint(0, max-1)


def position_aleatoire():
    x = entier_aleatoire(largeur_zone) * unite
    y = entier_aleatoire(hauteur_zone) * unite
    return x, y


def afficher_message(message: str, couleur: tuple):
    fenetre.blit(pygame.font.SysFont("comicsansms", 20).render(message, True, couleur), [0, 0])


def dans_zone(position: tuple):
    if 0 <= position[0] < largeur_zone*unite and 0 <= position[1] < hauteur_zone*unite:
        return True
    else:  # pas necessaire mais plus clair
        return False


def tour_de_jeu(fin_jeu):

    # v3 utilisation d'un bonus obstacle
    # Deux variables globales qui permettent d'activer le début du chronomètre quand:
    #Le joueur a atteind un score multiple de 5, on active un compteur permettant d'assurer un affichage de la pomme dorée à 5s
    global depart_pomme_doree
    #Si le serpent touche la pomme dorée, les obstacles deviennent des pommes pendant 5s
    global bonus_pomme_active
    


    afficher_message(str(len(serpent.positions)-1), noir)
    serpent.draw(fenetre)
    pomme.draw(fenetre)

    # Affichage obstacle
    if(len(obstacle.pos_obstacle) > 0): 
        obstacle.draw_obstacle(fenetre)

    #V3 : début du compteur permettant d'afficher la pomme dorée quand le joueur a atteind un score  multiple de 5
    if(len(serpent.positions)-2 >= pomme_doree.frequence_or): 
        depart_pomme_doree = time.time()
        pomme_doree.pomme_or_active = True
        pomme_doree.frequence_or += 5
       
    #v3 : affichage de la pomme dorée
    if pomme_doree.pomme_or_active: 
        actuel_doree = time.time()
        
        if actuel_doree - depart_pomme_doree < affichage_duree: 
            pomme_doree.draw(fenetre)
        else:
            pomme_doree.pomme_or_active = False

    #v3 : Si la pomme dorée est mangée par le serpent, alors le bonus est activé pendant 3 secondes
    if pomme_doree.pomme_or_active and pomme_doree.rect.collidepoint(serpent.positions[-1]): 
        obstacle.change_bonus = True
        pomme_doree.pomme_or_active = False
        bonus_pomme_active = time.time()
    
    #v3 : QUand le serpent a mangé la pomme dorée, il peut manger les "obstacles" pendant le laps de temps défini avant
    if obstacle.change_bonus: 
        #v3 : Debut de chrono
        timer_bonus_actuel = time.time()
        #v3 :  Tant que le temps écoulé pour l'affichage de la pomme est inféreiur à 3s, le serpent est "invincible"
        if(timer_bonus_actuel - bonus_pomme_active < 3): 
            #v3 : Si le serpent touche un des "obstacles" transformés en pommes : 
            if serpent.positions[-1] in obstacle.pos_obstacle: 
                #v3 : On retire l'obstacle du jeu et on augmente la taille du serpent
                obstacle.pos_obstacle.pop(obstacle.pos_obstacle.index(serpent.positions[-1]))
                #Juste pour le challenge, à chaque fois qu'un bonus est mangé alors on augmente le nombre d'obstacle par 1
                obstacle.max_obstacles += 1
                serpent.update()
                
            obstacle.draw_obstacle(fenetre)
        else:
            #v3 : Une fois le temps de bonus écoulé, on désactive l'"invicibilité du serpent"
            obstacle.change_bonus = False
        
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            fin_jeu = True
        if event.type == pygame.KEYDOWN:
            serpent.set_direction(event.key)
    serpent.update()


    # Une condition d'ajout d'obstacle de manière aléatoire quand le nombre d'obstacles ne dépasse le nombre maximale
    if(len(serpent.positions)-2 >= obstacle.frequene_obstacle): 
        obstacle.update_new()
        obstacle.frequene_obstacle += 5
        

    
    # À chaque fois qu'un score est divisible par 10 on augmente le nombre d'ajout d'obstacle
    if(len(serpent.positions)-2 >= obstacle.frequene_obstacle_nombre): 
        obstacle.num_obstacles +=1
        obstacle.frequene_obstacle_nombre += 8
    # À chaque fois qu'un score est divisible par 10 on permute les obstacles
    if(len(serpent.positions)-2 >= obstacle.frequene_obstacle_perm): 
        obstacle.permute_obstacles()
        obstacle.frequene_obstacle_perm += 10

    
    if not dans_zone(serpent.positions[-1]):
        fin_jeu = True


    if obstacle.collide_obstacle(serpent.positions[-1]) and not obstacle.change_bonus:
      
        fin_jeu = True
    if pomme.rect.collidepoint(serpent.positions[-1]):
        pomme.reposition()
    else:
        serpent.positions.pop(0)
    return fin_jeu


def dessine_fond():
    for y in range(0, hauteur_zone):
        for x in range(0, largeur_zone):
            color = vert_clair if (x + y) % 2 == 0 else vert_moins_clair
            pygame.draw.rect(fenetre, color, pygame.Rect(x * unite, y * unite, unite, unite))


# Couleurs
blanc = (255, 255, 255)
jaune = (255, 255, 102)
noir = (0, 0, 0)
rouge = (213, 50, 80)
vert = (0, 255, 0)
bleu = (50, 153, 213)
vert_clair = (176, 230, 40)
vert_moins_clair = (147, 199, 17)

# Variables
unite = 20
largeur_zone = 60
hauteur_zone = 40
couleur_nourriture = rouge
couleur_serpent = vert
couleur_fond = blanc

# SPRITES
SPRITE_SERPENT_TETE_DROITE = scale(pygame.image.load("sprites/base_game/head_right.png"), (unite, unite))
SPRITE_SERPENT_TETE_GAUCHE = scale(pygame.image.load("sprites/base_game/head_left.png"), (unite, unite))
SPRITE_SERPENT_TETE_HAUT = scale(pygame.image.load("sprites/base_game/head_up.png"), (unite, unite))
SPRITE_SERPENT_TETE_BAS = scale(pygame.image.load("sprites/base_game/head_down.png"), (unite, unite))
SPRITE_SERPENT_CORPS_HORIZONTAL = scale(pygame.image.load("sprites/base_game/body_horizontal.png"), (unite, unite))
SPRITE_SERPENT_CORPS_VERTICAL = scale(pygame.image.load("sprites/base_game/body_vertical.png"), (unite, unite))
SPRITE_SERPENT_CORPS_GAUCHE_BAS = scale(pygame.image.load("sprites/base_game/body_bottomleft.png"), (unite, unite))
SPRITE_SERPENT_CORPS_GAUCHE_HAUT = scale(pygame.image.load("sprites/base_game/body_topleft.png"), (unite, unite))
SPRITE_SERPENT_CORPS_BAS_DROITE = scale(pygame.image.load("sprites/base_game/body_bottomright.png"), (unite, unite))
SPRITE_SERPENT_CORPS_HAUT_DROITE = scale(pygame.image.load("sprites/base_game/body_topright.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_DROITE = scale(pygame.image.load("sprites/base_game/tail_right.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_GAUCHE = scale(pygame.image.load("sprites/base_game/tail_left.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_HAUT = scale(pygame.image.load("sprites/base_game/tail_up.png"), (unite, unite))
SPRITE_SERPENT_QUEUE_BAS = scale(pygame.image.load("sprites/base_game/tail_down.png"), (unite, unite))
SPRITE_POMME = scale(pygame.image.load("sprites/base_game/apple.png"), (unite, unite))
SPRITE_OBSTACLE = scale(pygame.image.load("sprites/base_game/obstacle.png"), (unite, unite))
SPRITE_GOLDEN = scale(pygame.image.load("sprites/base_game/golden_bonus.png") , (unite , unite))
# Jeu
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load("sons/CHILL.mp3")
pygame.mixer.music.set_volume(0.1)
pygame.mixer.music.play(-1)
fenetre = pygame.display.set_mode((largeur_zone*unite, hauteur_zone*unite))
pygame.display.set_caption("Snake")
fin_jeu = False
direction_serpent = (0, 0)
pomme = Pomme()
pomme_doree = Pomme_Doree()
serpent = Serpent()
obstacle = Obstacle()
clock = pygame.time.Clock()
tick = 18
affichage_duree = 5

while not fin_jeu:
    
    dessine_fond()
    fin_jeu = tour_de_jeu(fin_jeu)
    clock.tick(tick)
print("Game Over")
pygame.quit()
quit()
