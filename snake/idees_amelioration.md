# Pistes Amélioration Snake

## Fenêtre qui se ferme pas à la fin du jeu

## Ajouter de la musique, des SOUND FX

Bouts de code et doc utiles

## Mettre un sprite sur le snake et/ou la pomme

TODO

## Mettre les scores

- Préparer un API/serveur et leur faire faire le code de connexion et d'envoi des scores.
- Garder les scores en local avec la BDD Replit

## Faire un bot serpent ennemi

Avec des mouvements prédéfinis et qui doit être évité

Ou système probabiliste

## Rajouter des murs

Placements aléatoires, prédéfinis

## Bonus/Malus

Augmenter/Réduire la vitesse

Rendre capable de manger les autres serpents

Pomme empoisonnée qui réduit la taille

## Système de mise en pause du jeu

## Checkpoints

## Mode 2 joueurs

Vérifier si c'est jouable

### Mode 2 joueurs + pong avec les serpents

## Se téléporter en arrivant sur un bord

## Mourir si on se mord la queue

