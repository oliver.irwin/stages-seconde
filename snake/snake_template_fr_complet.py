# RUBIKA - SNAKE GAME

# Importation des bibliothèques, ne pas modifier
import pygame
import random


# Initialisation des couleurs, ne pas modifier
blanc = (255, 255, 255)
jaune = (255, 255, 102)
noir = (0, 0, 0)
rouge = (213, 50, 80)
vert = (0, 255, 0)
bleu = (50, 153, 213)

# Etape 1: Définition des variables
unite = 10
largeur_zone = 60
hauteur_zone = 40
couleur_nourriture = rouge
couleur_serpent = vert
couleur_fond = blanc


# Etape 2.3: Dessiner un carré dans la fenêtre, remplacer '...' par les bonnes variables
def dessine_carre(x: int, y: int, couleur: tuple):
    pygame.draw.rect(surface=fenetre, color=couleur, rect=(x, y, unite, unite))


# Etape 4: Dessiner plusieurs carrés à partir d'une liste de position et d'une couleur
def dessine_liste_carres(liste_positions: list, couleur: tuple):
    for position in liste_positions:
        dessine_carre(position[0], position[1], couleur)


# Etape 3.1: Obtenir un nombre aléatoire entre 0 et max (exclus)
def entier_aleatoire(max: int):
    return random.randint(0, max-1)


# Etape 3.2: Obtenir des coordinées aléatoire dans la fenêtre
def position_aleatoire():
    x = entier_aleatoire(largeur_zone) * unite
    y = entier_aleatoire(hauteur_zone) * unite
    return x, y


# Affiche un message dans le coin supérieur gauche de la fenêtre en Comic Sans MS, taille 20
def afficher_message(message: str, couleur: tuple):
    fenetre.blit(pygame.font.SysFont("comicsansms", 20).render(message, True, couleur), [0, 0])


# Etape 7: Obtenir la nouvelle direction en fonction de la touche appuyée
def direction(touche: pygame.key, direction_actuelle: tuple):
    if touche == pygame.K_UP:
        direction_future = (0, -1)
    elif touche == pygame.K_DOWN:
        direction_future = (0, 1)
    elif touche == pygame.K_LEFT:
        direction_future = (-1, 0)
    elif touche == pygame.K_RIGHT:
        direction_future = (1, 0)
    else:
        direction_future = direction_actuelle
    return direction_future


# Etape 9: Obtenir la nouvelle position en fonction de la direction actuelle
def nouvelle_position(positions_serpent: list, direction_serpent: tuple) -> tuple:
    position_tete = positions_serpent[-1]
    position_future = (position_tete[0] + direction_serpent[0]*unite, position_tete[1] + direction_serpent[1]*unite)  # simplifiable mais plus clair comme ça
    return position_future


# Etape 10: Vérifier si la postion est située dans la fenêtre
def dans_zone(position: tuple):
    if 0 <= position[0] < largeur_zone*unite and 0 <= position[1] < hauteur_zone*unite:
        return True
    else:  # pas necessaire mais plus clair
        return False


# Un tour de jeu
def tour_de_jeu(positions_serpent, fin_jeu, direction_serpent, position_pomme):
    # Etape 6.1: Afficher la pomme
    dessine_carre(position_pomme[0], position_pomme[1], couleur_nourriture)

    # Etape 6.2: Afficher le score
    afficher_message(str(len(positions_serpent)-1), noir)

    # Etape 6.3: Afficher le serpent
    dessine_liste_carres(positions_serpent, couleur_serpent)

    # Mise à jour de la fenêtre
    pygame.display.update()

    # Récupération des touches appuyées
    for event in pygame.event.get():
        # Etape 8.1: On a cliqué sur la croix, on ferme le jeu
        if event.type == pygame.QUIT:
            fin_jeu = True

        # Etape 8.2: Une touche est appuyée, on met à jour la direction
        if event.type == pygame.KEYDOWN:
            direction_serpent = direction(event.key, direction_serpent)

    # Etape 11: Quelle est la future position du serpent ? Est-ce dans la zone ?
    position_future = nouvelle_position(positions_serpent, direction_serpent)
    if dans_zone(position_future):
        positions_serpent.append(position_future)
    else:
        fin_jeu = True

    # Etape 12: Le serpent est-il sur une pomme ?
    if positions_serpent[-1] == position_pomme:
        position_pomme = position_aleatoire()
    else:
        positions_serpent = positions_serpent[1:]

    return positions_serpent, fin_jeu, direction_serpent, position_pomme


# Initialisation de la fenêtre de jeu
pygame.init()  # ne pas modifier

# Etape 2.1: Remplacer (..., ...) par les bonnes variables
fenetre = pygame.display.set_mode((largeur_zone*unite, hauteur_zone*unite))

# Définition du nom de la fenêtre
pygame.display.set_caption("Snake")

# Etape 5: Définition des variables d'environnement du jeu, remplacer les '...' par les bonne valeurs
positions_serpent = [(largeur_zone*unite//2, hauteur_zone*unite//2)]
fin_jeu = False
direction_serpent = (0, 0)
position_pomme = position_aleatoire()

# Définition de l'horloge interne du jeu, modifiez 'tick' pour augmenter ou diminuer la vitesse du jeu
clock = pygame.time.Clock()  # ne pas modifier
tick = 10

# Etape 6.4: Tant que le jeu n'est pas fini, remplacer '...' par la bonne variable
while fin_jeu == False:  # pas la meilleure syntaxe mais plus simple pour les stagiaires
    # Etape 2.2: On remplit le fond d'une couleur, remplacer '...' par la bonne variable
    fenetre.fill(couleur_fond)

    # Mise à jour des variables du jeu
    positions_serpent, fin_jeu, direction_serpent, position_pomme = tour_de_jeu(positions_serpent, fin_jeu, direction_serpent, position_pomme)
    clock.tick(tick)  # ne pas modifier

# Fin du jeu
print("Game Over")
pygame.quit()
quit()
